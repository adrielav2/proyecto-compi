# Implementa el veficador de ciruelas

from utils.arbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo
from utils.tipo_datos import TipoDatos

class TablaSímbolos:
    """ 
    Almacena información auxiliar para decorar el árbol de sintáxis
    abstracta con información de tipo y alcance.

    La estructura de símbolos es una lista de diccionarios 
    """

    def __init__(self):
        self.profundidad = 0
        self.símbolos = []
        self.errores = []

    def abrir_bloque(self):
        """
        Inicia un bloque de alcance (scope)
        """
        self.profundidad += 1

    def cerrar_bloque(self):
        """
        Termina un bloque de alcance y al hacerlo elimina todos los
        registros de la tabla que estan en ese bloque
        """

        for registro in self.símbolos:
            if registro['profundidad'] == self.profundidad:
                self.símbolos.remove(registro)

        self.profundidad -= 1

    def nuevo_registro(self, nodo, nombre_registro=''):
        """
        Introduce un nuevo registro a la tabla de símbolos
        """
        # El nombre del identificador + el nivel de profundidad 

        """
        Los atributos son: nombre, profundidad, referencia

        referencia es una referencia al nodo dentro del árbol
        """

        diccionario = {
            'nombre': nodo.contenido,
            'profundidad': self.profundidad,
            'referencia': nodo
        }

        self.símbolos.append(diccionario)

    def verificar_existencia(self, nombre):
        """
        Verifica si un identificador existe cómo variable/función global o local
        """
        for registro in self.símbolos:

            # si es local
            if registro['nombre'] == nombre and \
                    registro['profundidad'] <= self.profundidad:

                return registro
            

        self.errores.append(f'Esta variable o función no fue declarada correctamente: {nombre}')
        return

    def __str__(self):

        resultado = 'TABLA DE SÍMBOLOS\n\n'
        resultado += 'Profundidad: ' + str(self.profundidad) +'\n\n'
        for registro in self.símbolos:
            resultado += str(registro) + '\n'

        return resultado
    
    def obtener_errores(self):
        """
        Devuelve la lista de errores acumulados
        """
        return self.errores


class Visitante:

    tabla_símbolos: TablaSímbolos

    def __init__(self, nueva_tabla_símbolos):
        self.tabla_símbolos = nueva_tabla_símbolos
        self.errores = [] 

    def visitar(self, nodo :TipoNodo):
        
        visit_method = getattr(self, f"_Visitante__visitar_{nodo.tipo.name.lower()}", None)
        if visit_method:
            visit_method(nodo)
        else:
            self.errores.append(f'No existe un método para visitar nodos de tipo: {nodo.tipo}')
            return None

    def __visitar_programa(self, nodo_actual):
        """
        Programa ::= (Comentario | Asignación | Función)* Principal
        """
        for nodo in nodo_actual.nodos:
            # acá 'self' quiere decir que al método 'visitar' le paso el
            # objeto visitante que estoy usando.
            nodo.visitar(self)

    def __visitar_asignación(self, nodo_actual):
        """
        Asignación ::= Identificador metale (Identificador | Literal | ExpresiónMatemática | Invocación )
        """
        # Metó la información en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_símbolos.nuevo_registro(nodo_actual.nodos[0])

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Si es una función verifico el tipo que retorna para incluirlo en
        # la asignación y si es un literal puedo anotar el tipo (TIPO)

        if nodo_actual.nodos[1].atributos == {}:
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA
            nodo_actual.nodos[0].atributos['tipo'] = TipoDatos.CUALQUIERA
        else:
            nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']
            nodo_actual.nodos[0].atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']


    def __visitar_expresión_matemática(self, nodo_actual):
        """
        ExpresiónMatemática ::= (Expresión) | Número | Identificador

        """
        contiene_texto = False
        for nodo in nodo_actual.nodos:
            # Verifico que exista si es un identificador (IDENTIFICACIÓN) o una expresión
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)
                nodo.atributos['tipo'] = registro['referencia'].atributos['tipo']
                if registro is None:
                    return
                if nodo.atributos['tipo'] == TipoDatos.TEXTO:
                    mensaje_error = f"Hay un texto dentro de un identificador en una expresión matemática, se espera un número: {nodo.contenido}"
                    self.errores.append(mensaje_error)
                    contiene_texto = True
            elif nodo.tipo == TipoNodo.TEXTO:
                mensaje_error = f"Hay un texto en una expresión, se espera un número: {nodo.contenido}"
                self.errores.append(mensaje_error)
                contiene_texto = True
            elif nodo.tipo == TipoNodo.EXPRESIÓN:
                # Verificar recursivamente la subexpresión
                self.__visitar_expresión_matemática(nodo)
            else:
                nodo.visitar(self)

            if not contiene_texto:
                nodo.atributos['tipo'] = TipoDatos.NÚMERO
                
            else:
                nodo.atributos['tipo'] = TipoDatos.TEXTO
        return nodo
        




    def __visitar_expresión(self, nodo_actual):
        """
        Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática
        """
        
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Anoto el tipo de datos 'NÚMERO' (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_función(self, nodo_actual):
        """
        Función ::= (Comentario)? mae Identificador (ParámetrosFunción) BloqueInstrucciones
        """

        # Meto la función en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_símbolos.nuevo_registro(nodo_actual)

        self.tabla_símbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        self.tabla_símbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[2].atributos['tipo']


    def __visitar_invocación(self, nodo_actual):
        """
        Invocación ::= Identificador ( ParámetrosInvocación )
        """

        # Verfica que el 'Identificador' exista (IDENTIFICACIÓN) y que sea
        registro = self.tabla_símbolos.verificar_existencia(nodo_actual.nodos[0].contenido)

        if registro is None:
            return

        if registro['referencia'].tipo != TipoNodo.FUNCIÓN:
            self.errores.append('Esto es una variable, no función', registro)
 
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # El tipo resultado de la invocación es el tipo inferido de una
        # función previamente definida
        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']


    def __visitar_parámetros_invocación(self, nodo_actual):
        """
        ParámetrosInvocación ::= Valor (/ Valor)+
        """

        # Recordemos que 'Valor' no existe en el árbol...

        # Si es 'Identificador' verifico que exista (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:

            # Si existe y no es función ya viene con el tipo por que
            # fue producto de una asignación
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                self.tabla_símbolos.verificar_existencia(nodo.contenido)

            elif nodo.tipo == TipoNodo.FUNCIÓN:
                self.errores.append('Esto es una función', nodo.contenido) 

            # Si es número o texto nada más los visito
            nodo.visitar(self)

        # No hay tipos en los parámetros... se sabe en tiempo de ejecución


    def __visitar_parámetros_función(self, nodo_actual):
        """
        ParámetrosFunción ::= Identificador (/ Identificador)+
        """
        # Registro cada 'Identificador' en la tabla
        for nodo in nodo_actual.nodos:
                self.tabla_símbolos.nuevo_registro(nodo)
                nodo.visitar(self)


    def __visitar_instrucción(self, nodo_actual):
        """
        Instrucción ::= (Repetición | Bifurcación | (Asignación | Invocación) | Retorno | Error | Comentario )
        """

        # Visita la instrucción 

        nodo_actual.nodos[0].visitar(self)

    def __visitar_repetición(self, nodo_actual):
        """
        Repetición ::= upee Negación? (Condición) BloqueInstrucciones
        """
        index = 0
        if nodo_actual.nodos[index].tipo == TipoNodo.NEGACIÓN:
            self.__visitar_negación(nodo_actual.nodos[index])
            index += 1

        # Visita la condición
        self.__visitar_condición(nodo_actual.nodos[index])
        index += 1

        # Abre un nuevo bloque de símbolos
        self.tabla_símbolos.abrir_bloque()

        # Visita el bloque de instrucciones
        for nodo in nodo_actual.nodos[index:]:
            nodo.visitar(self)

        # Cierra el bloque de símbolos
        self.tabla_símbolos.cerrar_bloque()

        # Anota el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[index].atributos['tipo']


    def __visitar_bifurcación(self, nodo_actual):
        """
        Bifurcación ::= DiaySi (Sino)?
        """

        # Visita los dos nodos en el siguiente nivel si los hay
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

    def __visitar_diaysi(self, nodo_actual):
        """
        DiaySi ::= diaysiii Negación? (Condición) BloqueInstrucciones (Sino)?
        """
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.NEGACIÓN:
                self.__visitar_negación(nodo)
            elif nodo.tipo == TipoNodo.CONDICIÓN:
                self.__visitar_condición(nodo)

        # Abre un nuevo bloque de símbolos
        self.tabla_símbolos.abrir_bloque()

        # Visita el bloque de instrucciones
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.BLOQUE_INSTRUCCIONES:
                nodo.visitar(self)
            elif nodo.tipo == TipoNodo.SINO:
                self.__visitar_sino(nodo)

        # Cierra el bloque de símbolos
        self.tabla_símbolos.cerrar_bloque()

        nodo_actual.atributos['tipo'] = nodo_actual.nodos[0].atributos['tipo']

    def __visitar_sino(self, nodo_actual):
        """
        Sino ::= sino Negación? BloqueInstrucciones
        """
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.NEGACIÓN:
                self.__visitar_negación(nodo)

        # Abre un nuevo bloque de símbolos
        self.tabla_símbolos.abrir_bloque()

        # Visita el bloque de instrucciones
        for nodo in nodo_actual.nodos:
            if nodo.tipo != TipoNodo.NEGACIÓN:
                nodo.visitar(self)

        # Cierra el bloque de símbolos
        self.tabla_símbolos.cerrar_bloque()

        # Anota el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

    def __visitar_condición(self, nodo_actual):
        """
        Condición ::= Comparación ((divorcio|casorio) Comparación)? | Dentro
        """

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Comparación retorna un valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD


    def __visitar_comparación(self, nodo_actual):
        """
        Comparación ::= Valor Comparador Valor
        """
        # Si los 'Valor' son identificadores se asegura que existan (IDENTIFICACIÓN)
        registro1 = None
        registro2 = None
        for i, nodo in enumerate(nodo_actual.nodos):
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                if i == 0:
                    registro1 = self.tabla_símbolos.verificar_existencia(nodo.contenido)
                    if registro1 is None:
                        return
                elif i == 2:
                    registro2 = self.tabla_símbolos.verificar_existencia(nodo.contenido)
                    if registro2 is None:
                        return
            nodo.visitar(self)
        
        valor_izq = nodo_actual.nodos[0]
        comparador = nodo_actual.nodos[1]
        valor_der = nodo_actual.nodos[2]

        if valor_izq.tipo == TipoNodo.IDENTIFICADOR:
            valor_izq = registro1['referencia']

        if valor_der.tipo == TipoNodo.IDENTIFICADOR:
            valor_der = registro2['referencia']

        if valor_izq.atributos['tipo'] == valor_der.atributos['tipo']:
            nodo_actual.nodos[0].atributos['tipo'] = valor_izq.atributos['tipo']
            nodo_actual.nodos[2].atributos['tipo'] = valor_der.atributos['tipo']
            comparador.atributos['tipo'] = TipoDatos.COMPARADOR
            # Una comparación siempre tiene un valor de verdad
            nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

        elif valor_izq.atributos['tipo'] == TipoDatos.CUALQUIERA:
            if valor_der.atributos['tipo'] != TipoDatos.CUALQUIERA:
                nodo_actual.nodos[2].atributos['tipo'] = valor_der.atributos['tipo']
            comparador.atributos['tipo'] = TipoDatos.COMPARADOR
            nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD
        
        elif valor_der.atributos['tipo'] == TipoDatos.CUALQUIERA:
            if valor_izq.atributos['tipo'] != TipoDatos.CUALQUIERA:
                nodo_actual.nodos[0].atributos['tipo'] = valor_izq.atributos['tipo']
            comparador.atributos['tipo'] = TipoDatos.COMPARADOR
            nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

        else:
            raise Exception('Papo, algo tronó acá', str(nodo_actual))

    def __visitar_valor(self, nodo_actual):
        """
        Valor ::= (Identificador | Literal)
        """
        # En realidad núnca se va a visitar por que lo saqué del árbol
        # duránte la etapa de análisis

    def __visitar_retorno(self, nodo_actual):
        """
        Retorno :: sarpe (Valor)?
        """

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)
        
        if nodo_actual.nodos == []:
            # Si no retorna un valor no retorna un tipo específico 
            nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

        else:

            for nodo in nodo_actual.nodos:

                nodo.visitar(self)

                if nodo.tipo == TipoNodo.IDENTIFICADOR:
                    # Verifico si valor es un identificador que exista (IDENTIFICACIÓN)
                    registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)

                    if registro:
                        # le doy al sarpe el tipo de retorno del identificador encontrado
                        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']
                    else:
                        # Si no existe el identificador, se omite esta parte y se continua
                        continue

                else:
                    # Verifico si es un Literal de que tipo es (TIPO)
                    nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

    def __visitar_error(self, nodo_actual):
        """
        Error ::= safis Valor
        """
        # Verifico si 'Valor' es un identificador que exista (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                self.tabla_símbolos.verificar_existencia(nodo.contenido)

        if nodo_actual.nodos[0].tipo == TipoNodo.TEXTO:
            nodo_actual.nodos[0].atributos['tipo'] = TipoDatos.TEXTO

        # Un safis imprime a stderr y sigue sin retornar nada
        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO 


    def __visitar_principal(self, nodo_actual):
        """
        Principal ::= (Comentario)?  (jefe | jefa) mae BloqueInstrucciones
        """
        # Este mae solo va a tener un bloque de instrucciones que tengo que
        # ir a visitar

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[0].atributos['tipo']

    def __visitar_bloque_instrucciones(self, nodo_actual):
        """
        BloqueInstrucciones ::= { Instrucción+ }
        """
        # Visita todas las instrucciones que contiene
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Acá yo debería agarrar el tipo de datos del Retorno si lo hay
        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO 

        for nodo in nodo_actual.nodos:
            if 'tipo' in nodo.atributos and nodo.atributos['tipo'] != TipoDatos.NINGUNO:
                nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

    def __visitar_operador(self, nodo_actual):
        """
        Operador ::= (echele | quitele | chuncherequee | desmadeje)
        """
        # Operador para trabajar con números (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_valor_verdad(self, nodo_actual):
        """
        ValorVerdad ::= (True | False)
        """
        # Valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

    def __visitar_comparador(self, nodo_actual):
        """
        Comparador ::= (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        """
        # Estos comparadores son numéricos  (TIPO) 
        # (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        if nodo_actual.contenido not in ['misma vara', 'otra vara' ]:
            nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

        else:
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA
            # Si no es alguno de esos puede ser Numérico o texto y no lo puedo
            # inferir todavía
    
    def __visitar_lista(self, nodo_actual):
        """
        Lista ::= [ElementoLista]
        """
        # Si la lista está vacía, asignamos el tipo de lista vacía
        if len(nodo_actual.nodos) == 0:
            nodo_actual.atributos['tipo'] = TipoDatos.LISTA
            return

        # Si la lista no está vacía, verificamos el único elemento que debe ser de tipo ELEMENTO_LISTA
        if len(nodo_actual.nodos) != 1 or nodo_actual.nodos[0].tipo != TipoNodo.ELEMENTO_LISTA:
            self.errores.append("La lista debe contener un único elemento de tipo ELEMENTO_LISTA o estar vacía")
            return

        # Visita el nodo ELEMENTO_LISTA
        self.__visitar_elemento_lista(nodo_actual.nodos[0])
        
        # Asigna el tipo LISTA a la lista actual
        nodo_actual.atributos['tipo'] = TipoDatos.LISTA
    
    def __visitar_elemento_lista(self, nodo_actual):
        """
        ElementoLista ::= Valor (, Valor)*
        """
        for nodo in nodo_actual.nodos:
            # Verifico si es un identificador que exista o una expresión válida
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)
                if registro is None:
                    self.errores.append(f"El identificador '{nodo.contenido}' no existe.")
                    return
                nodo.atributos['tipo'] = registro['referencia'].atributos['tipo']
            elif nodo.tipo == TipoNodo.TEXTO or nodo.tipo == TipoNodo.ENTERO:
                nodo.visitar(self)
            else:
                self.errores.append(f"Tipo de nodo '{nodo.tipo}' no válido en un elemento de lista.")

        # Asignar tipo de datos a la lista de elementos (podría ser un conjunto de tipos)
        nodo_actual.atributos['tipo'] = TipoDatos.LISTA_ELEMENTO

    def __visitar_acceso_lista(self, nodo_actual):
        """
        AccesoLista ::= Identificador [ Expresión ]
        """
        if len(nodo_actual.nodos) != 2 or nodo_actual.nodos[0].tipo != TipoNodo.IDENTIFICADOR or nodo_actual.nodos[1].tipo != TipoNodo.EXPRESIÓN:
            self.errores.append("Acceso a lista mal formado, debe tener un identificador y una expresión.")
            return

        identificador = nodo_actual.nodos[0]
        expresion = nodo_actual.nodos[1]


        # Verifico si el identificador existe y es de tipo LISTA
        registro = self.tabla_símbolos.verificar_existencia(identificador.contenido)
        if registro is None:
            self.errores.append(f"El identificador '{identificador.contenido}' no existe.")
            return
        
        if registro['referencia'].atributos['tipo'] != TipoDatos.LISTA:
            self.errores.append(f"El identificador '{identificador.contenido}' no es una lista.")
            return

        identificador.atributos['tipo'] = TipoDatos.LISTA

        # Verifico si la expresión es válida (debería ser un número entero para el índice)
        nodo = self.__visitar_expresión_matemática(expresion)
        if nodo.tipo != TipoNodo.ENTERO:
            self.errores.append(f"El índice de acceso a una lista debe ser un número, se uso: TEXTO")
            return

        expresion.atributos['tipo'] = TipoDatos.NÚMERO

        # Asignar tipo de datos al acceso a la lista (puede depender del tipo de elementos en la lista)
        nodo_actual.atributos['tipo'] = TipoDatos.LISTA_ELEMENTO
    
    def __visitar_negación(self, nodo_actual):
        """
        Negación ::= no
        """

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Comparación retorna un valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD
    
    def __visitar_dentro(self, nodo_actual):
        """
        Dentro ::= Valor aentro Lista
        """

        # Visitar los nodos hijos
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Verificar y asignar tipos
        tipo_valor = nodo_actual.nodos[0].atributos['tipo']  # Tipo de Valor
        tipo_lista = nodo_actual.nodos[1].atributos['tipo']  # Tipo de Lista

        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

    def __visitar_texto(self, nodo_actual):
        # Texto (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.TEXTO

    def __visitar_entero(self, nodo_actual):
        # Entero (TIPO) 
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_flotante(self, nodo_actual):
        # Flotante (TIPO) 
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_identificador(self, nodo_actual):
        """
        Identificador ::= [a-z][a-zA-Z0-9]+
        """
        nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA

    def obtener_errores(self):
        return self.errores


class Verificador:
    asa            : ÁrbolSintáxisAbstracta
    visitador      : Visitante
    tabla_símbolos : TablaSímbolos

    def __init__(self, nuevo_asa: ÁrbolSintáxisAbstracta):

        self.asa = nuevo_asa

        self.tabla_símbolos = TablaSímbolos()
        self.__cargar_ambiente_estándar()

        self.visitador = Visitante(self.tabla_símbolos)

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """
            
        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def __cargar_ambiente_estándar(self):

        funciones_estandar = [ ('hacer_menjunje', TipoDatos.NINGUNO),
                ('viene_bolita', TipoDatos.TEXTO),
                ('trone', TipoDatos.NÚMERO),
                ('sueltele', TipoDatos.NINGUNO),
                ('echandi_jiménez', TipoDatos.TEXTO),
                ('esNum', TipoDatos.VALOR_VERDAD),
                ('todos_numeros', TipoDatos.VALOR_VERDAD),
                ('haga_lista', TipoDatos.LISTA),
                ('traiga_elemento', TipoDatos.CUALQUIERA),
                ('metale_elemento', TipoDatos.NINGUNO),
                ('saque_elemento', TipoDatos.NINGUNO)
                ]

        for nombre, tipo in  funciones_estandar:
            nodo = NodoÁrbol(TipoNodo.FUNCIÓN, contenido=nombre, atributos= {'tipo': tipo})
            self.tabla_símbolos.nuevo_registro(nodo)

    def verificar(self):
        self.visitador.visitar(self.asa.raiz)

        errores_tabla_símbolos = self.tabla_símbolos.obtener_errores()
        errores_visitante = self.visitador.obtener_errores()
        todos_los_errores = errores_tabla_símbolos + errores_visitante

        if todos_los_errores:
            print("Errores encontrados:")
            for error in todos_los_errores:
                print(error)
        else:
            print("No se encontraron errores.")






