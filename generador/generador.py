# Implementa el veficador de ciruelas

from utils.arbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo
from generador.visitadores import VisitantePython

class Generador:

    asa            : ÁrbolSintáxisAbstracta
    visitador      : VisitantePython

    ambiente_estandar = """import sys

def hacer_menjunje(texto1, texto2):
    return texto1 + texto2

def viene_bolita(texto, indice):
    return texto[indice]

def trone(objeto):
    return len(objeto)

def sueltele(texto):
    print(texto)

def echandi_jiménez():
    return input()

def esNum(numero):
    try:
        int(numero)
        return True
    except ValueError:
        return False

def haga_lista(*elementos):
    return list(elementos)

def traiga_elemento(lista, indice):
    return lista[indice]

def metale_elemento(lista, elemento):
    lista.append(elemento)

def saque_elemento(lista, indice):
    return lista.pop(indice)
"""

    def __init__(self, nuevo_asa: ÁrbolSintáxisAbstracta):

        self.asa            = nuevo_asa
        self.visitador      = VisitantePython() 

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """
            
        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def generar(self):
        resultado = self.visitador.visitar(self.asa.raiz)
        print(self.ambiente_estandar)
        print(resultado)


