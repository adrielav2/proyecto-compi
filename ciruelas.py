# Archivo principal para correr el compilador

from utils import archivos as utils
from explorador.explorador import Explorador
from analizador.analizador import Analizador  

import argparse

parser = argparse.ArgumentParser(description='Interprete para Ciruelas (el lenguaje)')

parser.add_argument('--solo-explorar', dest='explorador', action='store_true', 
        help='ejecuta solo el explorador y retorna una lista de componentes lexicos')

parser.add_argument('--solo-analizar', dest='analizador', action='store_true', 
        help='ejecuta hasta el analizador y retorna un preorden del árbol sintáctico')

parser.add_argument('archivo',
        help='Archivo de codigo fuente')

def ciruelas():

    args = parser.parse_args()

    if args.explorador is True: 

        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        exp.imprimir_componentes()

    elif args.analizador is True: 

        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        
        analizador = Analizador(exp.componentes)
        analizador.analizar()
        analizador.imprimir_asa()

    else:
        parser.print_help()


if __name__ == '__main__':
    ciruelas()

