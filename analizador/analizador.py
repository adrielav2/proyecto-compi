# Analizador de Ciruelas (el lenguaje de programación)

from explorador.explorador import TipoComponente, ComponenteLéxico
from utils.arbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo

class Analizador:

    componentes_léxicos : list
    cantidad_componentes: int
    posición_componente_actual : int
    componente_actual : ComponenteLéxico

    def __init__(self, lista_componentes):

        self.componentes_léxicos = lista_componentes
        self.cantidad_componentes = len(lista_componentes)

        self.posición_componente_actual = 0
        self.componente_actual = lista_componentes[0]

        self.asa = ÁrbolSintáxisAbstracta()

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """
            
        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()


    def analizar(self):
        """
        Método principal que inicia el análisis siguiendo el esquema de
        análisis por descenso recursivo
        """
        self.asa.raiz = self.__analizar_programa()


    def __analizar_programa(self):
        """
        Programa ::= (Comentario | Asignación | Función)* Principal
        """

        nodos_nuevos = []

        # Se ignoran los comentarios

        # pueden venir múltiples asignaciones o funciones
        while (True):

            # Si es asignación
            if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
                nodos_nuevos = [self.__analizar_asignación()]

            # Si es función
            elif (self.componente_actual.texto == 'mae'):
                nodos_nuevos += [self.__analizar_función()]

            else:
                break

        # Al final va una función principal
        if (self.componente_actual.texto in ['jefe', 'jefa']):
            nodos_nuevos += [self.__analizar_principal()]
        else:
            error_msg = f"Error de sintaxis: se esperaba 'jefe' o 'jefa', se encontró '{self.componente_actual.texto}'"
            raise ValueError(error_msg)

        
        return NodoÁrbol(TipoNodo.PROGRAMA, nodos=nodos_nuevos)
        

    def __analizar_asignación(self):
        """
        Asignación ::= Identificador metale ( Literal | ExpresiónMatemática | Lista | (Invocación | Identificador) )

        Se cambia la gramática nuevamente para incluir listas.
        """

        nodos_nuevos = []

        # El identificador en esta posición es obligatorio
        nodos_nuevos += [self.__verificar_identificador()]

        # Igual el métale
        self.__verificar('metale')

        # Verifica el tipo de valor asignado
        #AÑADIDO 
        if self.componente_actual.texto == '[':
            nodos_nuevos += [self.__analizar_lista()]
        elif self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
            nodos_nuevos += [self.__analizar_literal()]
        elif self.componente_actual.texto == '(':
            nodos_nuevos += [self.__analizar_expresión()]
        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            if self.__componente_venidero().texto == '(':
                nodos_nuevos += [self.__analizar_invocación()]
            else:
                nodos_nuevos += [self.__verificar_identificador()]
        else:
            print(f"Error: no se puede analizar la asignación del siguiente componente {self.componente_actual}")
            exit() 

        return NodoÁrbol(TipoNodo.ASIGNACIÓN, nodos=nodos_nuevos)


    def __analizar_expresión(self):
        """
        Expresión ::= (ExpresiónMatematica) | Número | Identificador | AccesoLista
        """

        nodos_nuevos = []
        
        # Primera opción
        if self.componente_actual.texto == '(':

            # Los verificar no se incluyen por que son para forzar cierta
            # forma de escribir, pero no aportan nada a la semántica
            self.__verificar('(')

            nodos_nuevos += [self.__analizar_expresión_matemática()]

            self.__verificar(')')
        
            
        # Acá yo se que estan bien formados por que eso lo hizo el explorador, lo que necesita revisar son las posiciones.
        elif self.componente_actual.tipo == TipoComponente.ENTERO:
            nodos_nuevos += [self.__verificar_entero()]

        elif self.componente_actual.tipo == TipoComponente.FLOTANTE:
            nodos_nuevos += [self.__verificar_flotante()]
        
        elif self.__componente_venidero().texto == '[':
            # Si es un acceso a lista, lo toma para hacer la expresión matematica
            nodos_nuevos += [self.__analizar_acceso_lista()]

        # Este código se simplifica si invierto la opción anterior y esta
        else:
            nodos_nuevos += [self.__verificar_identificador()]

        return NodoÁrbol(TipoNodo.EXPRESIÓN, nodos=nodos_nuevos)


    def   __analizar_expresión_matemática(self):
        """
        ExpresiónMatemática ::= Expresión Operador Expresión
        """

        nodos_nuevos = []

        # Acá no hay nada que hacer todas son obligatorias en este orden,

        nodos_nuevos += [self.__analizar_expresión()]

        nodos_nuevos += [self.__verificar_operador()]

        nodos_nuevos += [self.__analizar_expresión()]

        return NodoÁrbol(TipoNodo.EXPRESIÓN_MATEMÁTICA , nodos=nodos_nuevos)

    def __analizar_función(self):
        """
        Función ::= (Comentario)? mae Identificador (ParámetrosFunción) BloqueInstrucciones
        """

        nodos_nuevos = []
        # Comentario se ignora.

        # Sección obligatoria en este orden.
        self.__verificar('mae')

        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_parámetros_función()]
        self.__verificar(')')
        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        # La función lleva el nombre del identificador
        return NodoÁrbol(TipoNodo.FUNCIÓN, \
                contenido=nodos_nuevos[0].contenido, nodos=nodos_nuevos)

    def __analizar_invocación(self):
        """
        Invocación ::= Identificador ( ParámetrosInvocación )
        """
        nodos_nuevos = []

        #todos son obligatorios en ese orden
        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_parámetros_invocación()]
        self.__verificar(')')

        return NodoÁrbol(TipoNodo.INVOCACIÓN , nodos=nodos_nuevos)

    def __analizar_parámetros_función(self):
        """
        ParametrosFunción ::= Identificador (/ Identificador)+
        """
        nodos_nuevos = []

        # Fijo un valor tiene que haber
        nodos_nuevos += [self.__verificar_identificador()]

        while( self.componente_actual.texto == '/'):
            self.__verificar('/')
            nodos_nuevos += [self.__verificar_identificador()]

        # Esto funciona con lógica al verrís... Si no revienta con error
        # asumimos que todo bien y seguimos.

        return NodoÁrbol(TipoNodo.PARÁMETROS_FUNCIÓN , nodos=nodos_nuevos)

    def __analizar_parámetros_invocación(self):
        """
        ParametrosInvocación ::= Valor | AccesoLista
        """
        nodos_nuevos = []

        # Verifica si el siguiente componente es un identificador
        if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            # Si es un identificador, verifica si le sigue un corchete "[" para determinar si es un acceso a lista
            #AÑADIDO
            if self.__componente_venidero().texto == '[':
                # Si es un acceso a lista, lo analiza y lo añade a la lista de parámetros
                nodos_nuevos += [self.__analizar_acceso_lista()]
            else:
                # Si no es un acceso a lista, analiza el identificador como un valor individual
                nodos_nuevos += [self.__analizar_valor()]
        # Si es un valor individual (distinto de un identificador seguido de "["), lo añade a la lista de parámetros
        elif self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
            nodos_nuevos += [self.__analizar_valor()]
        else:
            # Si no es ninguno de los casos anteriores, se produce un error de sintaxis
            print(f'Error: Se esperaba un valor individual o un acceso a lista en {self.componente_actual}')
            exit()  # Termina el programa

        # Verifica si hay más parámetros separados por "/"
        # AÑADIDO
        while self.componente_actual.texto == '/':
            self.__verificar('/')
            # Analiza el siguiente parámetro
            if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
                if self.__componente_venidero().texto == '[':
                    nodos_nuevos += [self.__analizar_acceso_lista()]
                else:
                    nodos_nuevos += [self.__analizar_valor()]
            elif self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
                nodos_nuevos += [self.__analizar_valor()]
            else:
                print(f"Error: Se esperaba un valor individual o un acceso a lista: {self.componente_actual}")
                exit()

        return NodoÁrbol(TipoNodo.PARÁMETROS_INVOCACIÓN , nodos=nodos_nuevos)

    
    def __analizar_instrucción(self):
        """
        Instrucción ::= (Repetición | Bifurcación | (Asignación | Invocación) | Retorno | Error | Comentario | AccesoLista | Expresión )

                                                    ^                       ^
        Ojo los paréntesis extra                    |                       |
        """

        nodos_nuevos = []        


        # Acá todo con if por que son opcionales
        if self.componente_actual.texto == 'upee':
            nodos_nuevos += [self.__analizar_repetición()]

        elif self.componente_actual.texto == 'diaysiii':
            nodos_nuevos += [self.__analizar_bifurcación()]

        elif self.componente_actual.texto == "(":
                nodos_nuevos += [self.__analizar_expresión()]

        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:

            if self.__componente_venidero().texto == 'metale':
                nodos_nuevos += [self.__analizar_asignación()]
            #AÑADIDO
            elif self.__componente_venidero().texto == '[':
                nodos_nuevos += [self.__analizar_acceso_lista()]
            else:
                nodos_nuevos += [self.__analizar_invocación()]

        elif self.componente_actual.texto == 'sarpe':
            nodos_nuevos += [self.__analizar_retorno()]

        else: # Muy apropiado el chiste de ir a revisar si tiene error al último.
            nodos_nuevos += [self.__analizar_error()]

        # Ignorado el comentario

        return NodoÁrbol(TipoNodo.INSTRUCCIÓN, nodos=nodos_nuevos)


    def __analizar_repetición(self):
        """
        Repetición ::= upee Negación? ( Condición | Iteración ) BloqueInstrucciones
        """
        nodos_nuevos = []

        # Los parentesis y el upe presentes de forma obligatoria.
        self.__verificar('upee')
        #AÑADIDO Posibilidad de negación.
        if self.componente_actual.texto == 'no':
            nodos_nuevos += [self.__analizar_negación()]

        self.__verificar('(')


        #AÑADIDO, posiblidad de comparación o iteración
        if self.__componente_venidero().tipo == TipoComponente.ASIGNACION:
            nodos_nuevos += [self.__analizar_iteración()] 
        else:
            nodos_nuevos += [self.__analizar_condición()]

        self.__verificar(')')

        # Yo acá tengo dos elecciones... creo otro nivel con Bloque de
        # instrucciones o pongo directamente las instrucciones en este
        # nivel... yo voy con la primera por facilidad... pero eso hace más
        # grande el árbol
        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoÁrbol(TipoNodo.REPETICIÓN, nodos=nodos_nuevos)
    
    #AÑADIDO
    def __analizar_iteración(self):
        """
        Iteración ::= Identificador metale Valor; Comparación; Identificador échele Valor.
        """

        nodos_nuevos = []
        nodos_nuevos += [self.__analizar_asignación()]

        self.__verificar(';')

        nodos_nuevos += [self.__analizar_comparación()]

        self.__verificar(';')

        nodos_nuevos += [self.__analizar_expresión()]

        return NodoÁrbol(TipoNodo.ITERACIÓN, nodos=nodos_nuevos)
        

    def __analizar_bifurcación(self):
        """
        Bifurcación ::= DiaySi (Sino)?
        """
        nodos_nuevos = []

        # el sino es opcional
        nodos_nuevos += [self.__analizar_diaysi()]

        if self.componente_actual.texto == 'sino':
            nodos_nuevos += [self.__analizar_sino()]

        # y sino era solo el 'diay siii'
        return NodoÁrbol(TipoNodo.BIFURCACIÓN, nodos=nodos_nuevos)

    def __analizar_diaysi(self):
        """
        DiaySi ::= diaysiii Negación? ( Condición ) BloqueInstrucciones
        """
        nodos_nuevos = []

        # Todos presentes en ese orden de forma obligatoria.
        self.__verificar('diaysiii')

        # AÑADIDO
        # Verifica si hay negación
        if self.componente_actual.texto == 'no':
            nodos_nuevos += [self.__analizar_negación()]

        self.__verificar('(')
        nodos_nuevos += [self.__analizar_condición()]
        self.__verificar(')')

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoÁrbol(TipoNodo.DIAYSI, nodos=nodos_nuevos)

    def __analizar_sino(self):
        """
        Sino ::= sino Negación? BloqueInstrucciones
        """

        nodos_nuevos = []

        # Todos presentes en ese orden... sin opciones
        self.__verificar('sino')

        if self.componente_actual.texto == 'no':
            nodos_nuevos += [self.__analizar_negación()]

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoÁrbol(TipoNodo.SINO, nodos=nodos_nuevos)

 

    def __analizar_condición(self):
        """
        Condición ::= Comparación ((divorcio | casorio) Comparación)? | Dentro | Invocación
        """
        nodos_nuevos = []

        # Verifica si es una comparación o la regla "Dentro"
        # AÑADIDO
        if self.__componente_venidero().texto == 'aentro':
            # Si es "aentro", entonces es la regla "Dentro"
            nodos_nuevos += [self.__analizar_dentro()]
        elif self.__componente_venidero().tipo == TipoComponente.COMPARADOR:
            #Se verifica si es un comparador.
            nodos_nuevos += [self.__analizar_comparación()]
        else:
            # Si no es ninguno de los dos, es una invocación.
            nodos_nuevos += [self.__analizar_invocación()]

            # Verifica si hay una cláusula adicional con "divorcio" o "casorio"
            if self.componente_actual.tipo == TipoComponente.PALABRA_CLAVE:
                if self.componente_actual.texto == 'divorcio':
                    nodo = NodoÁrbol(TipoNodo.OPERADOR_LÓGICO, contenido='divorcio')
                    nodos_nuevos += [nodo]
                    self.__verificar('divorcio')
                else:
                    nodo = NodoÁrbol(TipoNodo.OPERADOR_LÓGICO, contenido='casorio')
                    nodos_nuevos += [nodo]
                    self.__verificar('casorio')

                # Analiza la segunda comparación
                nodos_nuevos += [self.__analizar_comparación()]

        return NodoÁrbol(TipoNodo.CONDICIÓN, nodos=nodos_nuevos)
    
    #AÑADIDO
    def __analizar_dentro(self):
        """
        Dentro ::= Valor aentro Identificador
        """
        nodos_nuevos = []

        # Verifica el valor
        nodos_nuevos += [self.__analizar_valor()]

        # Verifica la palabra clave "aentro"
        self.__verificar('aentro')

        # Verifica la lista
        nodos_nuevos += [self.__verificar_identificador()]

        return NodoÁrbol(TipoNodo.DENTRO, nodos=nodos_nuevos)

    def __analizar_comparación(self):
        """
        Comparación ::= Valor Comparador Valor
        """
        nodos_nuevos = []

        # Sin opciones, todo se analiza
        nodos_nuevos += [self.__analizar_valor()]
        nodos_nuevos += [self.__verificar_comparador()]
        nodos_nuevos += [self.__analizar_valor()]

        return NodoÁrbol(TipoNodo.COMPARACIÓN, nodos=nodos_nuevos)
    
    def __analizar_negación(self):
        """
        Negación ::= no
        """
        nodos_nuevos = []

        self.__verificar('no')

        return NodoÁrbol(TipoNodo.NEGACIÓN, nodos=nodos_nuevos)

    def __analizar_valor(self):
        """
        Valor ::= (Identificador | Literal)
        """

        # El uno o el otro
        if self.componente_actual.tipo is TipoComponente.IDENTIFICADOR:
            nodo = self.__verificar_identificador()
        else:
            nodo = self.__analizar_literal()

        return nodo

    def __analizar_retorno(self):
        """
        Retorno :: sarpe (Valor)?
        """
        nodos_nuevos = []

        self.__verificar('sarpe')

        # Este hay que validarlo para evitar el error en caso de que no
        # aparezca
        if self.componente_actual.tipo in [TipoComponente.IDENTIFICADOR, TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO] :
            nodos_nuevos += [self.__analizar_valor()]

        # Sino todo bien...
        return NodoÁrbol(TipoNodo.RETORNO, nodos=nodos_nuevos)

    def __analizar_error(self):
        """
        Error ::= safis Valor
        """
        nodos_nuevos = []        

        # Sin opciones
        self.__verificar('safis')
        nodos_nuevos += [self.__analizar_valor()]

        return NodoÁrbol(TipoNodo.ERROR, nodos=nodos_nuevos)
    
    def __analizar_lista(self):
        """
        Lista ::= [ElementoLista]
        """
        nodos_nuevos = []

        # Verifica el inicio de la lista
        self.__verificar('[')

        nodos_nuevos += [self.__analizar_elemento_lista()]

        # Verifica los elementos de la lista separados por comas
        while self.componente_actual.texto == ',':
            self.__verificar(',')
            nodos_nuevos += [self.__analizar_elemento_lista()]

        # Verifica el cierre de la lista
        self.__verificar(']')

        return NodoÁrbol(TipoNodo.LISTA, nodos=nodos_nuevos)

    def __analizar_elemento_lista(self):
        """
        ElementoLista ::= Valor (, Valor)*
        """
        nodos_nuevos = []

        # Se analiza el primer valor
        nodos_nuevos += [self.__analizar_valor()]

        # Se verifica la presencia de más valores separados por comas
        while self.componente_actual.texto == ',':
            self.__verificar(',')
            nodos_nuevos += [self.__analizar_valor()]

        return NodoÁrbol(TipoNodo.ELEMENTO_LISTA, nodos=nodos_nuevos)

    def __analizar_acceso_lista(self):
        """
        AccesoLista ::= Identificador [ Expresión ]
        """
        nodos_nuevos = []

        # Se verifica el identificador de la lista
        nodos_nuevos += [self.__verificar_identificador()]

        # Se verifica el inicio del acceso
        self.__verificar('[')

        # Se analiza la expresión dentro del acceso
        nodos_nuevos += [self.__analizar_expresión()]

        # Se verifica el cierre del acceso
        self.__verificar(']')

        return NodoÁrbol(TipoNodo.ACCESO_LISTA, nodos=nodos_nuevos)


    def __analizar_principal(self):
        """
        Esta versión esta chocha por que no cumple con ser una gramática LL
        Principal ::= (Comentario)?  mae (jefe | jefa) BloqueInstrucciones
        """
        nodos_nuevos = []
        # Ignora comentarios

        if self.componente_actual.texto == 'jefa':
            self.__verificar('jefa')
        else:
            self.__verificar('jefe')

        self.__verificar('mae')

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoÁrbol(TipoNodo.PRINCIPAL, nodos=nodos_nuevos)


    def __analizar_literal(self):
        """
        Literal ::= (Número | Texto | ValorVerdad)
        """

        # Acá le voy a dar vuelta por que me da pereza tanta validación
        if self.componente_actual.tipo is TipoComponente.TEXTO:
            nodo = self.__verificar_texto()

        elif  self.componente_actual.tipo is TipoComponente.VALOR_VERDAD:
            nodo = self.__verificar_valor_verdad()

        else:
            nodo = self.__analizar_número()

        return nodo

    def __analizar_número(self):
        """
        Número ::= (Entero | Flotante)
        """
        if self.componente_actual.tipo == TipoComponente.ENTERO:
            nodo = self.__verificar_entero()
        else:
            nodo = self.__verificar_flotante()

        return nodo

    def __analizar_bloque_instrucciones(self):
        """
        Este es nuevo y me lo inventé para simplicicar un poco el código...
        correspondería actualizar la gramática.

        BloqueInstrucciones ::= { Instrucción+ }
        """
        nodos_nuevos = []

        # Obligatorio
        self.__verificar('{')

        # mínimo una
        nodos_nuevos += [self.__analizar_instrucción()]

        # Acá todo puede venir uno o más 
        while self.componente_actual.texto in ['upee', 'diaysiii', 'sarpe', 'safis'] \
                or self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
        
            nodos_nuevos += [self.__analizar_instrucción()]

        # Obligatorio
        self.__verificar('}')

        return NodoÁrbol(TipoNodo.BLOQUE_INSTRUCCIONES, nodos=nodos_nuevos)

# Todos estos verificar se pueden unificar =*=
    def __verificar_operador(self):
        """
        Operador ::= (echele | quitele | chuncherequee | desmadeje)
        """   
        self.__verificar_tipo_componente(TipoComponente.OPERADOR)

        nodo = NodoÁrbol(TipoNodo.OPERADOR, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()

        return nodo

    def __verificar_valor_verdad(self):
        """
        ValorVerdad ::= (True | False)
        """
        self.__verificar_tipo_componente(TipoComponente.VALOR_VERDAD)

        nodo = NodoÁrbol(TipoNodo.VALOR_VERDAD, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo

    def __verificar_comparador(self):
        """
        Comparador ::= (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        """
        self.__verificar_tipo_componente(TipoComponente.COMPARADOR)

        nodo = NodoÁrbol(TipoNodo.COMPARADOR, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo

    def __verificar_texto(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo TEXTO

        Texto ::= " /\w(\s\w)*)? "
        """
        self.__verificar_tipo_componente(TipoComponente.TEXTO)

        nodo = NodoÁrbol(TipoNodo.TEXTO, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo


    def __verificar_entero(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo ENTERO

        Entero ::= (-)?\d+
        """
        self.__verificar_tipo_componente(TipoComponente.ENTERO)

        nodo = NodoÁrbol(TipoNodo.ENTERO, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo


    def __verificar_flotante(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo FLOTANTE

        Flotante ::= (-)?\d+.(-)?\d+
        """
        self.__verificar_tipo_componente(TipoComponente.FLOTANTE)

        nodo = NodoÁrbol(TipoNodo.FLOTANTE, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo


    def __verificar_identificador(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo
        IDENTIFICADOR

        Identificador ::= [a-z][a-zA-Z0-9]+
        """
        self.__verificar_tipo_componente(TipoComponente.IDENTIFICADOR)

        nodo = NodoÁrbol(TipoNodo.IDENTIFICADOR, contenido =self.componente_actual.texto)
        self.__pasar_siguiente_componente()
        return nodo


    def __verificar(self, texto_esperado ):

        """
        Verifica si el texto del componente léxico actual corresponde con
        el esperado cómo argumento
        """

        if self.componente_actual.texto != texto_esperado:
            print(f"Error: Se esperaba {texto_esperado}, pero se encontró {str(self.componente_actual)}")
            exit()

        self.__pasar_siguiente_componente()


    def __verificar_tipo_componente(self, tipo_esperado ):
        """
        Verifica un componente por tipo... no hace mucho pero es para
        centralizar el manejo de errores
        """

        if self.componente_actual.tipo is not tipo_esperado:
            print()
            print(f"Error: Se esperaba {tipo_esperado}, pero se encontró {str(self.componente_actual)}")
            exit()



    def __pasar_siguiente_componente(self):
        """
        Pasa al siguiente componente léxico

        Esto revienta por ahora
        """
        self.posición_componente_actual += 1

        if self.posición_componente_actual >= self.cantidad_componentes:
            return

        self.componente_actual = \
                self.componentes_léxicos[self.posición_componente_actual]


    def __componente_venidero(self, avance=1):
        """
        Retorna el componente léxico que está 'avance' posiciones más
        adelante... por default el siguiente. Esto sin adelantar el
        contador del componente actual.
        """
        return self.componentes_léxicos[self.posición_componente_actual+avance]
    
    def __componente__doble_venidero(self, avance=2):
        """
        Retorna el componente léxico que está 'avance' posiciones más
        adelante... por default el siguiente. Esto sin adelantar el
        contador del componente actual.
        """
        return self.componentes_léxicos[self.posición_componente_actual+avance]

