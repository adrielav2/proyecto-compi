# Implementa el generador de ciruelas

from utils.arbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo

class VisitantePython:
    
    tabuladores = 0
    errores = []

    def visitar(self, nodo: TipoNodo):
        """
        Método principal que redirige la visita según el tipo de nodo.
        """

        if nodo.tipo is TipoNodo.PROGRAMA:
            return self.__visitar_programa(nodo)
        
        elif nodo.tipo is TipoNodo.ASIGNACIÓN:
            return self.__visitar_asignación(nodo)
        
        elif nodo.tipo is TipoNodo.EXPRESIÓN_MATEMÁTICA:
            return self.__visitar_expresión_matemática(nodo)
        
        elif nodo.tipo is TipoNodo.EXPRESIÓN:
            return self.__visitar_expresión(nodo)
        
        elif nodo.tipo is TipoNodo.FUNCIÓN:
            return self.__visitar_función(nodo)
        
        elif nodo.tipo is TipoNodo.INVOCACIÓN:
            return self.__visitar_invocación(nodo)
        
        elif nodo.tipo is TipoNodo.PARÁMETROS_INVOCACIÓN:
            return self.__visitar_parámetros_invocación(nodo)
        
        elif nodo.tipo is TipoNodo.PARÁMETROS_FUNCIÓN:
            return self.__visitar_parámetros_función(nodo)
        
        elif nodo.tipo is TipoNodo.INSTRUCCIÓN:
            return self.__visitar_instrucción(nodo)
        
        elif nodo.tipo is TipoNodo.REPETICIÓN:
            return self.__visitar_repetición(nodo)
        
        elif nodo.tipo is TipoNodo.BIFURCACIÓN:
            return self.__visitar_bifurcación(nodo)
        
        elif nodo.tipo is TipoNodo.DIAYSI:
            return self.__visitar_diaysi(nodo)
        
        elif nodo.tipo is TipoNodo.SINO:
            return self.__visitar_sino(nodo)
        
        elif nodo.tipo is TipoNodo.OPERADOR_LÓGICO:
            return self.__visitar_operador(nodo)
        
        elif nodo.tipo is TipoNodo.CONDICIÓN:
            return self.__visitar_condición(nodo)
        
        elif nodo.tipo is TipoNodo.COMPARACIÓN:
            return self.__visitar_comparación(nodo)
        
        elif nodo.tipo is TipoNodo.RETORNO:
            return self.__visitar_retorno(nodo)
        
        elif nodo.tipo is TipoNodo.ERROR:
            return self.__visitar_error(nodo)
        
        elif nodo.tipo is TipoNodo.PRINCIPAL:
            return self.__visitar_principal(nodo)
        
        elif nodo.tipo is TipoNodo.BLOQUE_INSTRUCCIONES:
            return self.__visitar_bloque_instrucciones(nodo)
        
        elif nodo.tipo is TipoNodo.OPERADOR:
            return self.__visitar_operador(nodo)
        
        elif nodo.tipo is TipoNodo.VALOR_VERDAD:
            return self.__visitar_valor_verdad(nodo)
        
        elif nodo.tipo is TipoNodo.COMPARADOR:
            return self.__visitar_comparador(nodo)
        
        elif nodo.tipo is TipoNodo.TEXTO:
            return self.__visitar_texto(nodo)
        
        elif nodo.tipo is TipoNodo.ENTERO:
            return self.__visitar_entero(nodo)
        
        elif nodo.tipo is TipoNodo.FLOTANTE:
            return self.__visitar_flotante(nodo)
        
        elif nodo.tipo is TipoNodo.IDENTIFICADOR:
            return self.__visitar_identificador(nodo)

        elif nodo.tipo is TipoNodo.LISTA:
            return self.__visitar_lista(nodo)

        elif nodo.tipo is TipoNodo.ACCESO_LISTA:
            return self.__visitar_accesolista(nodo)

        elif nodo.tipo is TipoNodo.ELEMENTO_LISTA:
            return self.__visitar_elementolista(nodo)

        elif nodo.tipo is TipoNodo.DENTRO:
            return self.__visitar_dentro(nodo)

        else:
            raise Exception('En realidad nunca va a llegar acá')


    def __visitar_programa(self, nodo_actual):
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return '\n'.join(instrucciones)

    def __visitar_asignación(self, nodo_actual):
        resultado = "{} = {}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(instrucciones[0], instrucciones[1])

    def __visitar_expresión_matemática(self, nodo_actual):
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return ' '.join(instrucciones)

    def __visitar_expresión(self, nodo_actual):
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return ' '.join(instrucciones)

    def __visitar_función(self, nodo_actual):
        resultado = "\ndef {}({}):\n{}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(instrucciones[0], instrucciones[1], '\n'.join(instrucciones[2]))

    def __visitar_invocación(self, nodo_actual):
        resultado = "{}({})"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(instrucciones[0], instrucciones[1])

    def __visitar_parámetros_invocación(self, nodo_actual):
        parámetros = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return ','.join(parámetros)

    def __visitar_parámetros_función(self, nodo_actual):
        parámetros = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return ','.join(parámetros)

    def __visitar_instrucción(self, nodo_actual):
        valor = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return ''.join(valor)

    def __visitar_repetición(self, nodo_actual):
        resultado = "while {}:\n{}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(instrucciones[0], '\n'.join(instrucciones[1]))

    def __visitar_bifurcación(self, nodo_actual):
        resultado = "{}{}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(instrucciones[0], instrucciones[1] if len(instrucciones) > 1 else '')

    def __visitar_diaysi(self, nodo_actual):
        if nodo_actual.nodos[0].tipo is TipoNodo.NEGACIÓN:
            resultado = """if not {}:\n{}"""
            instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos[1:]]
            return resultado.format(instrucciones[0],'\n'.join(instrucciones[1]))
        else:
           resultado = """if {}:\n{}"""
           instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
           return resultado.format(instrucciones[0],'\n'.join(instrucciones[1]))
        

    def __visitar_sino(self, nodo_actual):
        resultado = "else:\n{}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format('\n'.join(instrucciones))

    def __visitar_condición(self, nodo_actual):
        resultado = "{} {} {}"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        if len(instrucciones) == 1:
            return instrucciones[0]
        else:
            return resultado.format(instrucciones[0], instrucciones[1], instrucciones[2])

    def __visitar_comparación(self, nodo_actual):
        resultado = "{} {} {}"
        elementos = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        return resultado.format(elementos[0], elementos[1], elementos[2])

    def __visitar_retorno(self, nodo_actual):
        resultado = "return {}"
        valor = ''.join([self.visitar(nodo) for nodo in nodo_actual.nodos])
        return resultado.format(valor)

    def __visitar_error(self, nodo_actual):
        """
        Error ::= safis Valor
        """
        resultado = 'print("\\033[91m", {}, "\\033[0m", file=sys.stderr)'
        valor = self.visitar(nodo_actual.nodos[0])

        # Escapar las comillas dobles en el valor
        valor = valor.replace('"', '\\"')
        
        return resultado.format(f'"{valor}"')


    def __visitar_principal(self, nodo_actual):
        resultado = "\ndef principal():\n{}\n\nif __name__ == '__main__':\n    principal()"
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        # Aplanar la lista de instrucciones si alguna es una lista
        instrucciones_planas = [instruccion if isinstance(instruccion, str) else '\n'.join(instruccion) for instruccion in instrucciones]
        return resultado.format('\n'.join(instrucciones_planas))

    def __visitar_bloque_instrucciones(self, nodo_actual):
        self.tabuladores += 4
        instrucciones = [self.visitar(nodo) for nodo in nodo_actual.nodos]
        instrucciones_tabuladas = [self.__retornar_tabuladores() + instruccion for instruccion in instrucciones]
        self.tabuladores -= 4
        return instrucciones_tabuladas

    def __visitar_operador(self, nodo_actual):
        operadores = {
            'echele': '+',
            'quitele': '-',
            'chuncherequee': '*',
            'desmadeje': '/'
        }
        return operadores.get(nodo_actual.contenido, '')

    def __visitar_valor_verdad(self, nodo_actual):
        return nodo_actual.contenido

    def __visitar_comparador(self, nodo_actual):
        comparadores = {
            'cañazo': '>',
            'poquitico': '<',
            'misma vara': '==',
            'otra vara': '!=',
            'menos o igualitico': '<=',
            'más o igualitico': '>='
        }
        return comparadores.get(nodo_actual.contenido, '')
    
    def __visitar_lista(self, nodo_actual):
        """
        Lista ::= [ElementoLista]
        """
        elementos = [nodo.visitar(self) for nodo in nodo_actual.nodos]
        return f"[{''.join(elementos)}]"

    def __visitar_elementolista(self, nodo_actual):
        """
        ElementoLista ::= Valor (, Valor)*
        """
        elementos = [nodo.visitar(self) for nodo in nodo_actual.nodos]
        return ', '.join(elementos)

    def __visitar_accesolista(self, nodo_actual):
        """
        AccesoLista ::= Identificador [ Expresión ]
        """
        identificador = nodo_actual.nodos[0].visitar(self)
        expresion = nodo_actual.nodos[1].visitar(self)
        return f"{identificador}[{expresion}]"


    def __visitar_valor(self, nodo_actual):
        """
        Valor ::= (Identificador | Literal)
        """
        return nodo_actual.visitar(self)

    def __visitar_negacion(self, nodo_actual):
        """
        Negación ::= no
        """
        return "not"

    def __visitar_dentro(self, nodo_actual):
        """
        Dentro ::= Valor aentro Lista
        """
        valor = nodo_actual.nodos[0].visitar(self)
        lista = nodo_actual.nodos[1].visitar(self)
        return f"{valor} in {lista}"

    def __visitar_texto(self, nodo_actual):
        return nodo_actual.contenido.replace('~', '"')

    def __visitar_entero(self, nodo_actual):
        return nodo_actual.contenido

    def __visitar_flotante(self, nodo_actual):
        return nodo_actual.contenido

    def __visitar_identificador(self, nodo_actual):
        return nodo_actual.contenido

    def __retornar_tabuladores(self):
        return " " * self.tabuladores





