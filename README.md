# Proyecto Compi

Compilador de Ciruelas

Este es un proyecto de compilador para el lenguaje Ciruelas. Ciruelas es un lenguaje diseñado para propósitos educativos y de aprendizaje de conceptos de compilación. En esta iteración se añadieron elementos para mejorar el lenguaje como lo son negaciones, iteraciones tipo "for" y entre otros.

Descripción

Este compilador de Ciruelas es una implementación básica que incluye la gramática del lenguaje y un explorador para analizar el código fuente y producir una lista de componentes léxicos.

Correr Explorador 

Para ejecutar el explorador se usa el siguiente comando: 

python3 ciruelas.py --solo-explorar docs/ejemplos/prueba1.cl

Para ejecutar el analizador se usa el siguiente comando: 

python3 ciruelas.py --solo-analizar docs/ejemplos/prueba1.cl
