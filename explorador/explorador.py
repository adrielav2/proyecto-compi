# Explorador para el proyecto extensión de Ciruelas por CompilaTeam. Primera versión.
# Adriel Araya 2019312845
# Naomi Illama 2021114064
# Compiladores e Intrepretes
# Escuela de Computación, Instituto Tecnológico de Costa Rica

import re
from enum import Enum, auto

#Enum que define los tipos de componentes léxicos disponibles.
class TipoComponente(Enum):
    COMENTARIO = auto()
    PALABRA_CLAVE = auto()
    NEGACION = auto()
    CONDICIONAL = auto()
    REPETICION = auto()
    ASIGNACION = auto()
    OPERADOR = auto()
    COMPARADOR = auto()
    DENTRO = auto()
    TEXTO = auto()
    IDENTIFICADOR = auto()
    ENTERO = auto()
    FLOTANTE = auto()
    VALOR_VERDAD = auto()
    PUNTUACION = auto()
    BLANCOS = auto()

#Clase que representa un componente léxico con su tipo y texto
class ComponenteLéxico:

    tipo    : TipoComponente
    texto   : str 

    def __init__(self, tipo: TipoComponente, texto: str, atributos: str):
        self.tipo = tipo
        self.texto = texto
        self.atributos = atributos

    #Devuelve una representación en texto del componente léxico    
    def __str__(self):
        tipo_sin_prefijo = self.tipo.name.split('.')[-1] #Para obtener el tipo sin prefijo.
        resultado = f'<"{tipo_sin_prefijo}", "{self.texto}", "{self.atributos}"> '
        return resultado
    
# Clase que realiza la exploración del código fuente en Ciruelas para identificar los componentes léxicos
class Explorador:
    descriptores_componentes = [
        (TipoComponente.COMENTARIO, r'^Bomba:\s(.*$)'),
        (TipoComponente.PALABRA_CLAVE, r'^(mae|sarpe|jefe|jefa|safis)'),
        (TipoComponente.NEGACION, r'^(no)'),
        (TipoComponente.CONDICIONAL, r'^(diaysiii|sino)'),
        (TipoComponente.REPETICION, r'^(upee)'),
        (TipoComponente.ASIGNACION, r'^(metale)'),
        (TipoComponente.OPERADOR, r'^(echele|quitele|chuncherequee|desmadeje)'),
        (TipoComponente.COMPARADOR, r'^(canazo|poquitico|misma vara|otra vara|menos o igualitico|mas o igualitico)'),
        (TipoComponente.DENTRO, r'^(aentro)'),
        (TipoComponente.TEXTO, r'^"(.*?[^"]*)"'),
        (TipoComponente.IDENTIFICADOR, r'^([a-z][a-zA-Z0-9_]*)'),
        (TipoComponente.ENTERO, r'^(-?[0-9]+)'),
        (TipoComponente.FLOTANTE, r'^(-?[0-9]+\.[0-9]+)'),
        (TipoComponente.VALOR_VERDAD, r'^(True|False)'),
        (TipoComponente.PUNTUACION, r'^([,;/{}()\[\]])'),
        (TipoComponente.BLANCOS, r'^(\s)+'), 
    ]

    def __init__(self, contenido_archivo):
        self.texto = contenido_archivo
        self.componentes = []

    #Explora el código fuente línea por línea y procesa cada línea para identificar los componentes léxicos
    def explorar(self):
        for numero_linea, linea in enumerate(self.texto, start=0):
            resultado = self.procesar_linea(linea,numero_linea)
            self.componentes.extend(resultado)

    #Imprime los componentes léxicos identificados
    def imprimir_componentes(self):
        for componente in self.componentes:
            print(componente)

    #Procesa una línea de código para identificar los componentes léxicos, levanta un error si existe algún elemento de la línea que coincida con algún elemento
    def procesar_linea(self, linea, numero_linea):
        componentes = []

        # Verificar si la línea comienza con el comentario "Bomba:" Sí es así no se procesa por ser un comentario.
        if "Bomba:" in linea:
            return componentes
        

    # Iterar sobre todos los tokens encontrados en la línea
        while linea:
            token_encontrado = False  # Bandera para verificar si se encontró un token

            # Iterar sobre todos los tipos de componente y sus expresiones regulares asociadas
            for tipo_componente, regex_tipo in self.descriptores_componentes:
                # Intentar hacer coincidir el token con la expresión regular del tipo de componente actual
                respuesta = re.match(regex_tipo, linea)
                if respuesta is not None:
                    token_encontrado = True
                    if tipo_componente is not TipoComponente.BLANCOS:
                        # Si no es un espacio en blanco, crea un nuevo componente léxico con el tipo y el texto encontrados, además del número de línea
                        nuevo_componente = ComponenteLéxico(tipo_componente, respuesta.group(), f'Número de línea: {numero_linea}')
                        componentes.append(nuevo_componente)
                    # Actualizar la línea para continuar desde el final del token encontrado
                    linea = linea[respuesta.end():].lstrip()  # Eliminar el token y los espacios en blanco iniciales
                    break

            # Si no se encontró ningún tipo de componente para el token actual, lanzar un error
            if not token_encontrado:
                raise ComponenteNoEncontradoError(linea, numero_linea, linea[0])

        # Devolver la lista de componentes léxicos encontrados en la línea
        return componentes

#Excepción personalizada para indicar que no se ha encontrado ningún componente léxico.
class ComponenteNoEncontradoError(Exception):
    def __init__(self, linea, numero_linea, elemento):
        self.linea = linea
        self.numero_linea = numero_linea 
        self.elemento = elemento
        super().__init__(f"Se encontró un componente léxico no definido en la línea: {numero_linea}. El elemento sin tipo es: {elemento}")
